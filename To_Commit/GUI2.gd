extends MarginContainer

onready var bar = $VBoxContainer/Life
onready var bio = $VBoxContainer/Bio
onready var nonbio = $VBoxContainer/Nonbio
onready var haz = $VBoxContainer/Haz
var Position
const Truck = preload("res://Truck.tscn")

var waste_type 
var bio_c : int
var non_c : int 
var haz_c : int
var bio_c2 : int
var non_c2: int 
var haz_c2 : int
var can_call = false

signal fill


func _ready():
	bar.max_value = 10
	bio.max_value = 4
	nonbio.max_value = 4
	haz.max_value = 4
	update_health(bar.max_value)

func update_health(new_value):
	bar.value = new_value
	
func _on_GUI2_fill():
	Position = get_parent().get_node("Viewport1/Main/Player2/Position2D/Camera2D/Truck_Spawn")
	var truck = Truck.instance()
	Position.add_child(truck)
	var player = get_parent().get_node("Viewport1/Main/Player2")
	player.player_health += 2
	update_health(player.player_health)

func _input(_event):
	if Input.is_action_just_pressed("truck2") && can_call == true:
		emit_signal("fill")
		bio.value = 0
		bio_c = 0
		nonbio.value = 0
		non_c = 0
		haz.value = 0
		haz_c = 0
		can_call = false
		


func _on_Player2_health_changed(health):
	update_health(health)


func _on_Player2_hit_type(type):
	if type == "Biodegradable":
		bio_c +=1
		bio.value = bio_c
	if type == "Non-biodegradable":
		non_c +=1
		nonbio.value = non_c
	if type == "Hazardous":
		haz_c +=1
		haz.value = haz_c
	if bio.value == bio.max_value or nonbio.value == nonbio.max_value or haz.value == haz.max_value:
		can_call = true
