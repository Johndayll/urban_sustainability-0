extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$VideoPlayer.play()
	var timer = Timer.new()
	timer.wait_time = 6
	$Roar.play()
	timer.connect("timeout", self ,"change_scene")
	timer.autostart = true
	add_child(timer)

func change_scene():
	get_tree().change_scene("res://To_Transfer/new_arena.tscn")
