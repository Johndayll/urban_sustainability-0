extends "motion.gd"

export(float) var SPEED = 700

func enter():
	update_look_direction(get_input_direction())
	owner.get_node("AnimationPlayer").play("walk")

func update(delta):
	var input_direction = get_input_direction()
	if not input_direction:
		emit_signal("finished", "idle")
		return
	update_look_direction(input_direction)

	owner.move(input_direction.normalized() * SPEED)
