extends KinematicBody2D

export(float) var speed = 85
onready var nav_2d = get_parent().get_node("Navigation2D")
onready var player = get_parent().get_node("Player")
onready var player2 = null
onready var cycle = Timer.new()
var path = []
var enemy_health = 3
var type
var stun = 0
var projectile_mode
onready var last_motion

var already_hit = false
var explosion = preload("res://explosions/Explosion.tscn").instance()
var is_exploding = false
var cycles : int = 0
var hit_group

func cycle_speed():
	cycles += 1
	if cycles % 2 == 1:
		speed = 1100
		cycle.set_wait_time(2)
	elif cycles % 2 == 0:
		speed = 85
		cycle.set_wait_time(.15)

func _ready():
	if Mode.mode == true:
		player2 = get_parent().get_node("Player2")
	cycle.set_wait_time(2)
	cycle.connect("timeout",self,"cycle_speed")
	add_child(cycle)
	cycle.autostart = true
	cycle.start()
	cycle.set_wait_time(.15)


func _update_navigation_path(start_position, end_position1, end_position2 = null):
	if end_position2 == null:
		path = nav_2d.get_simple_path(start_position, end_position1, false)
	else:
		if global_position.distance_to(end_position1) < global_position.distance_to(end_position2):
			path = nav_2d.get_simple_path(start_position, end_position1, false)
		else:
			path = nav_2d.get_simple_path(start_position, end_position2, false)
	path.remove(0)

func _physics_process(delta):
	if Mode.mode == true:
		_update_navigation_path(global_position, player.global_position,player2.global_position)
	else:
		_update_navigation_path(global_position, player.global_position)
	if stun == 0:
		get_node("Anim").modulate = Color.white
		move_along_path(speed * delta)
	else:
		get_node("Anim").modulate = Color.indianred
		move_and_slide(last_motion.normalized() * 1500)
		stun = stun - 1
		if type == projectile_mode && !already_hit:
			enemy_health -= 1
			already_hit = true
			if enemy_health == 0:
				if hit_group == 1:
					player.emit_signal("hit_type", type)
				elif hit_group == 2:
					player2.emit_signal("hit_type", type)
	
	if enemy_health <= 0 && !is_exploding:
		death()
		


func death():
	Mode.enemies -= 1
	explosion()
	Score_count.score += 40
	is_exploding = true

func solid():
	get_node("c").disabled = false
	get_node("hitbox/d").disabled = false

func ghost():
	get_node("c").disabled = true
	get_node("hitbox/d").disabled = true

func move_along_path(distance):
	global_position = global_position.linear_interpolate(path[0], distance / global_position.distance_to(path[0]))

func explosion():
	add_child(explosion)
