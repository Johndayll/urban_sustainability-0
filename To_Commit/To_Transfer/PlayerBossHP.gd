extends TextureProgress

func _ready():
	value = get_parent().player_max_health

func _on_Player_health_changed(health):
	value = get_parent().player_health
