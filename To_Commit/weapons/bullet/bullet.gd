extends "res://weapons/damage_areas/damage_source.gd"

export(PackedScene) var ImpactEffect
export(float) var SPEED = 1000.0

var direction = Vector2()

func _physics_process(delta):
	position += direction * SPEED * delta

func _on_body_entered(body):
	impact()

func _on_area_entered(area):
	impact()

func impact():
	var impact_node = ImpactEffect.instance()
	add_child(impact_node)
	impact_node.emitting = true
	get_tree().create_timer(impact_node.lifetime).connect("timeout", self, "queue_free")
	set_active(false)

func set_active(value):
	set_physics_process(value)
	$CollisionShape2D.disabled = value
	$Circle.visible = false
