extends Node2D

const enemy_path  = ["res://Enemies!/Bio/banana.tscn","res://Enemies!/Bio/paper.tscn","res://Enemies!/Haz/spray.tscn","res://Enemies!/Haz/wash.tscn","res://Enemies!/Nonbio/plastic.tscn","res://Enemies!/Nonbio/water.tscn"]
const enemies = [preload("res://Enemies!/Bio/banana.tscn"),preload("res://Enemies!/Bio/paper.tscn"),preload("res://Enemies!/Haz/spray.tscn"),preload("res://Enemies!/Haz/wash.tscn"),preload("res://Enemies!/Nonbio/plastic.tscn"),preload("res://Enemies!/Nonbio/water.tscn")]
var explosion = preload("res://explosions/Explosion.tscn").instance()
var timer = Timer.new()
var entity

func _ready():
	Mode.enemies = 0
	Mode.boss = true
	if Mode.mode == true:
		$Player2.global_position = $SpawnPoint.global_position
		$Player2.show()
		randomize()
		timer.wait_time = 7
		timer.connect("timeout", self ,"on_timeout_complete")
		timer.autostart = true
		add_child(timer)
		timer.start()
	
	
func on_timeout_complete():
	entity = randi() % 6
	var enemy = enemies[entity].instance()
	match enemy_path[entity][15]:
		'B':
			enemy.type = "Biodegradable"
		'N':
			enemy.type = "Non-biodegradable"
		'H':
			enemy.type = "Hazardous"
	entity = randi() % 4
	if Mode.enemies < 2:
		add_child(enemy)
		Mode.enemies += 1
	enemy.global_position = get_node(str("EnemySpawnPoint",entity)).global_position


