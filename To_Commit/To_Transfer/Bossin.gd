extends ColorRect

signal boss_start

func fade_in():
	$AnimationPlayer.play("boss_in")
	

func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("boss_start")
