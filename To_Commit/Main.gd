extends Node

onready var viewport1 = $Viewports/ViewportContainer1/Viewport1
onready var viewport2 = $Viewports/ViewportContainer2/Viewport2

func _ready():
	OS.window_fullscreen = true
	viewport2.world_2d = viewport1.world_2d
