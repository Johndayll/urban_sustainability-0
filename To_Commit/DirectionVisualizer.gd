extends Position2D


const SCALE_RANGE = Vector2(0.5, 1.0)
const SCALE_RATE = 4
var dir = Vector2()


func _ready():
	get_node("Arrow").scale = Vector2(SCALE_RANGE.x, SCALE_RANGE.x)

func _physics_process(_delta):
	var deadzone = .3
	var controllerangle
	var xAxisRL = Input.get_joy_axis(1,JOY_AXIS_2)
	var yAxisUD = Input.get_joy_axis(1,JOY_AXIS_3)
	if abs(xAxisRL) > deadzone || abs(yAxisUD) > deadzone:
		get_node("Arrow").show()
		controllerangle = Vector2(xAxisRL, yAxisUD).angle()
		dir = Vector2(cos(controllerangle), sin(controllerangle))
		rotation = controllerangle
	else:
		get_node("Arrow").hide()
