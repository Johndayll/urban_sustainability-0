extends KinematicBody2D
export var speed = 300
var sprite_direction = "down"
var sprite_lastdirection
var motion = Vector2()
var DAMAGE = 0
var timer = null
var timer2 = null
var switch_counter : int
var mode = "Biodegradable"
var delay = .25
var can_shoot = true
const Projectile = preload("res://Projectile.tscn")
var hitstun = 0
var knock_direction = Vector2.ZERO


var player_health = 0
var player_max_health = 100
var stun = 0
var trail_down
var velocity
var last_velocity
onready var trail = get_node("Trail")

signal health_changed(health)
signal death 
signal mode_changed
signal hit_type(type)
signal label_change(mode)
signal boss_time

func _init():
	player_health = player_max_health

func _ready():
	trail_down = Timer.new()
	trail_down.set_one_shot(true)
	trail_down.set_wait_time(1.2)
	add_child(trail_down)
	timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(delay)
	timer.connect("timeout", self ,"on_timeout_complete")
	add_child(timer)

func on_timeout_complete():
	can_shoot = true

func _physics_process(delta):
	
	var LEFT = -int(Input.is_action_pressed("ui2_left"))
	var RIGHT = int(Input.is_action_pressed("ui2_right"))
	var UP = -int(Input.is_action_pressed("ui2_up"))
	var DOWN = int(Input.is_action_pressed("ui2_down"))
	
	motion.x = LEFT + RIGHT
	
	if -LEFT + RIGHT == 2:
		if Input.is_action_just_pressed("ui2_left"):
			sprite_lastdirection = -1
		if Input.is_action_just_pressed("ui2_right"):
			sprite_lastdirection = 1
		motion.x = sprite_lastdirection
	
	motion.y = UP + DOWN  
	
	if -UP + DOWN == 2:
		if Input.is_action_just_pressed("ui2_up"):
			sprite_lastdirection = -1
		if Input.is_action_just_pressed("ui2_down"):
			sprite_lastdirection = 1
		motion.y = sprite_lastdirection
	speed = 300
	if hitstun > 0:
		get_node("hit").play()
		$Anim.modulate = Color.indianred
		velocity = knock_direction.normalized() * speed * 2
		hitstun -= 1
	else:
		get_node("hit").stop()
		$Anim.modulate = Color.white
		velocity = motion.normalized() * speed

	if stun > 0:
		if get_tree().get_nodes_in_group("enemys") != null:
			get_tree().call_group("enemys","ghost")
		speed = 1500
		trail.show()
		$dash.play()
		move_and_slide(last_velocity * speed * delta)
		stun -= 1
		
	else:
		if get_tree().get_nodes_in_group("enemys") != null:
			get_tree().call_group("enemys","solid")
		trail.hide()
		move_and_slide(velocity)
	match motion:
		Vector2.LEFT:
			sprite_direction = "left"
		Vector2.RIGHT:
			sprite_direction = "right"
		Vector2.UP:
			sprite_direction = "up"
		Vector2.DOWN:
			sprite_direction = "down"
	if motion == Vector2(0, 0):
		_animation_process("idle")
	else:
		_animation_process("walk")
	
	if Input.is_action_pressed("ui2_accept") && can_shoot && $DirectionVisualizer.dir != Vector2.ZERO:
		shoot()
		can_shoot = false
		timer.start()
		
	if player_health == 0 or player_health < 0:
		emit_signal("death")
	
	if Score_count.score >= 3000 && Score_count.score < 3300:
		
		emit_signal("boss_time")
		

sync func shoot():
	var projectile = Projectile.instance()
	projectile.mode = mode
	$shoot.play()
	projectile.hit_type = 2
	projectile.dir = get_node("DirectionVisualizer").dir
	projectile.global_position = self.global_position
	get_parent().add_child(projectile)
	
func _animation_process(sprite_state):
	var animation = str(sprite_state, sprite_direction)
	$Anim.play(animation)
	
	
func _on_hitbox_area_entered(area):
	if area.get("speed") == null && area.is_in_group("enemy_hitbox"):
		hitstun = 30
		if global_position > area.global_position:
			knock_direction = transform.origin - area.transform.origin
		else:
			knock_direction = -(transform.origin - area.transform.origin)
	if !area.get_parent().is_in_group("player") && !area.is_in_group("truck"):
		player_health -= 1
		emit_signal("health_changed", player_health)

func _on_Player_death():
	var slow = Timer.new()
	Engine.time_scale = .1
	slow.wait_time = .01
	slow.connect("timeout", self , "change_scene")
	slow.autostart = true
	add_child(slow)
	
func change_scene():
	if Mode.mode == true and Mode.boss == false:
		get_parent().get_parent().get_parent().get_parent().get_parent().get_node("Fade/Fadein").show()
		get_parent().get_parent().get_parent().get_parent().get_parent().get_node("Fade/Fadein").fade_in()
	elif Mode.mode == true or Mode.mode == false:
		get_parent().get_node("Fade/Fadein").show()
		get_parent().get_node("Fade/Fadein").fade_in()
		
func _on_Fadein_fade_finished():
	get_tree().change_scene("res://UI/Death.tscn")
func _input(event):
	if event.is_action_pressed("switch2_weapon"):
		emit_signal("mode_changed")
		emit_signal("label_change", mode)
		
	if event.is_action_pressed("dash2") && trail_down.is_stopped() && motion != Vector2.ZERO:
		trail_down.start()
		trail.restart()
		stun = 8
		var deadzone = .3
		var controllerangle
		var xAxisRL = Input.get_joy_axis(1,JOY_AXIS_0)
		var yAxisUD = Input.get_joy_axis(1,JOY_AXIS_1)
		if abs(xAxisRL) > deadzone || abs(yAxisUD) > deadzone:
			controllerangle = Vector2(xAxisRL, yAxisUD).angle()
			var dir = Vector2(cos(controllerangle), sin(controllerangle))
			last_velocity = dir.normalized() * (speed/2.2)
		else:
			last_velocity = motion.normalized() * (speed/2.2)
			
func _on_Player_mode_changed(): 
	switch_counter += 1
	if switch_counter > 2:
		switch_counter = 0
	match switch_counter:
		0: mode = "Biodegradable"
		1: mode = "Non-biodegradable"
		2: mode = "Hazardous"

func _on_Bossin_fade_finished():
	get_tree().change_scene("res://To_Transfer/Roar.tscn")


func _on_Player2_boss_time():
	if Mode.mode == false:
		get_parent().get_node("Fade/Bossin").show()
		get_parent().get_node("Fade/Bossin").fade_in()
	else:
		get_parent().get_parent().get_parent().get_parent().get_parent().get_node("Fade/Bossin").show()
		get_parent().get_parent().get_parent().get_parent().get_parent().get_node("Fade/Bossin").fade_in()

