extends CanvasLayer
const Truck = preload("res://Truck.tscn")
var Position 
const entity = preload("res://Entities/Player.tscn")

func _on_GUI_full():
	Position = get_parent().get_node("Player/Position2D/Camera2D/Truck_Spawn")
	var truck = Truck.instance()
	Position.add_child(truck)
	if Mode.mode == true:
		var player = get_parent().get_node("Viewport1/Main/Player")
		player.player_health += 2
		get_parent().get_node("CanvasLayer/GUI").update_health(player.player_health)
	else:
		var player = get_parent().get_node("Player")
		player.player_health += 2 
		get_node("GUI").update_health(player.player_health)
