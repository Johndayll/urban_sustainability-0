extends Label


func _ready():
	text = "Biodegradable"
	add_color_override("font_color", Color( 0.5, 1, 0, 1 ))

func _on_Player_label_change(mode):
	text = mode
	if mode == "Biodegradable":
		add_color_override("font_color", Color( 0.5, 1, 0, 1 ))
	if mode == "Non-biodegradable":
		add_color_override("font_color", Color.dodgerblue)
	if mode == "Hazardous":
		add_color_override("font_color", Color( 1, 1, 0, 1 ))
