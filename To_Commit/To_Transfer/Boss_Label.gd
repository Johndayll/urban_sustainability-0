extends Tween

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	interpolate_property(get_parent(),"modulate",Color.white,Color(1,1,1,0),3,Tween.TRANS_LINEAR,Tween.EASE_IN)
	start()
