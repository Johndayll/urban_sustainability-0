extends Node2D

var grid_position = Vector2()
var grid_size = OS.get_screen_size()

onready var parent = get_parent()

func _ready():
	# If you drag the camera from the OffsetPivot node,
	# its position will not be (0, 0)
	$Camera2D.make_current()
	$Camera2D.global_position = Vector2()
	if get_parent().get_parent().get_node("Player2") != null:
		grid_size = Vector2(OS.get_screen_size().x/2,OS.get_screen_size().y)
	else:
		grid_size = OS.get_screen_size()
	set_as_toplevel(true)
	update_grid_position()


func _physics_process(_delta):
	if Mode.boss == false:
		update_grid_position()


func update_grid_position():
	var new_grid_position = calculate_grid_position()
	if grid_position == new_grid_position:
		return
	grid_position = new_grid_position
	jump_to_grid_position()


func calculate_grid_position():
	var x = round(parent.global_position.x / grid_size.x)
	var y = round(parent.global_position.y / grid_size.y)
	if Input.is_action_pressed("look_left"):
		x = round(parent.global_position.x/ grid_size.x-1)
	if Input.is_action_pressed("look_right"):
		x = round(parent.global_position.x/ grid_size.x+1)
	if Input.is_action_pressed("look_down"):
		y = round(parent.global_position.y/ grid_size.y+1)
	if Input.is_action_pressed("look_up"):
		y = round(parent.global_position.y/ grid_size.y-1)
	return Vector2(x, y)


func jump_to_grid_position():
	global_position = grid_position * grid_size
