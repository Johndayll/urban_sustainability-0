extends KinematicBody2D
var speed = 95
var movedir = Vector2(0, 0)
var movetimer_length = 35
var movetimer = 0
var amal_health = 1
var already_hit = false
var stun = 0
var projectile_mode 
var last_motion 
var Class = "Amalgamation"
const Spread = preload("res://To_transfer/Amalgamation_pattern.tscn")
const Spread2 = preload("res://To_transfer/Amalgamation_pattern2.tscn")
var timer = Timer.new()
var is_exploding = false
var player
var player2
var hit_group

signal death

func _ready():
	player = get_parent().get_node("Player")
	player2 = get_parent().get_node("Player2")
	timer.one_shot = true
	timer.wait_time = 2
	timer.connect("timeout",self,"disintegrate")
	$Anim.play()
	var spawn = Timer.new()
	spawn.wait_time = 2.3
	spawn.connect("timeout", self , "shoot")
	spawn.autostart = true
	add_child(spawn)
	var timer2 = Timer.new()
	timer2.wait_time = 4.0
	timer2.connect("timeout", self , "shoot2")
	timer2.autostart = true
	add_child(timer2) 
func movement_loop():
	var pos
	if player.global_position < global_position:
		pos = (player.global_position - global_position).normalized()
	else:
		pos = (player2.global_position - global_position).normalized()
	var motion = pos * speed
	move_and_slide(motion)
func _physics_process(_delta):
	movement_loop()
	if movetimer > 0:
		movetimer -= 1
		
	if movetimer == 0 || is_on_wall():
		movedir = boss_direction.rand()
		movetimer = movetimer_length
	
	if amal_health <= 0:
		emit_signal("death")

func scene():
	get_parent().get_node("Fade/MainIn").show()
	get_parent().get_node("Fade/MainIn").fade_in()
func death():
	add_child(timer)
	timer.start()
	is_exploding = true

func shoot():
	var bullet = Spread.instance()
	add_child(bullet)
func shoot2():
	var bullet = Spread2.instance()
	add_child(bullet)

func _on_hitbox_area_entered(area):
	area.queue_free()
	amal_health -= 1

func _on_MainIn_fade_finished():
	Score_count.score += 500
	if Mode.mode == true:
		get_tree().change_scene("res://Main.tscn")
	else:
		get_tree().change_scene("res://new_map_new.tscn")


func _on_Amalgamation_death():
	var slow = Timer.new()
	Engine.time_scale = .1
	slow.wait_time = .001
	slow.connect("timeout", self , "scene")
	slow.autostart = true
	add_child(slow)
