extends MarginContainer

onready var bar = $VBoxContainer/Life
onready var bio = $VBoxContainer/Bio
onready var nonbio = $VBoxContainer/Nonbio
onready var haz = $VBoxContainer/Haz

var waste_type 
var bio_c : int
var non_c : int 
var haz_c : int
var bio_c2 : int
var non_c2: int 
var haz_c2 : int
var can_call = false
signal full
func _ready():
	bar.max_value = 10
	bio.max_value = 4
	nonbio.max_value = 4
	haz.max_value = 4
	update_health(bar.max_value)
func update_health(new_value):
	bar.value = new_value

func _on_Player_health_changed(player_health):
	update_health(player_health)
	

func _on_Player_hit_type(type):
	if type == "Biodegradable":
		bio_c +=1
		bio.value = bio_c
	if type == "Non-biodegradable":
		non_c +=1
		nonbio.value = non_c
	if type == "Hazardous":
		haz_c +=1
		haz.value = haz_c
	if bio.value == bio.max_value or nonbio.value == nonbio.max_value or haz.value == haz.max_value:
		can_call = true
func _input(_event):
	if Input.is_action_just_pressed("truck") && can_call == true:
		emit_signal("full")
		bio.value = 0
		bio_c = 0
		nonbio.value = 0
		non_c = 0
		haz.value = 0
		haz_c = 0
		can_call = false
		



